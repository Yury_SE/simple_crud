from django.apps import AppConfig


class EmplRegisterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'empl_register'
