from django.urls import path
from . import views

urlpatterns = [
    path('', views.empl_form, name='insert'),
    path('list/', views.empl_list, name='list'),
    path('form/', views.empl_form, name='form'),
    path('<int:empl_id>/', views.empl_form, name='update'),
    path('delete/<int:empl_id>/', views.empl_delete, name='delete'),
]
