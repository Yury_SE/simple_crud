from django.test import TestCase
from django.urls import reverse


class TemplateDisplayForm(TestCase):
    """Test templates viewing"""

    def setUp(self) -> None:
        url_form = reverse('form')
        self.response = self.client.get(url_form)

    def test_form_template(self):
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'employee_form.html')


class TemplateDisplayList(TestCase):
    """Test templates viewing"""

    def setUp(self) -> None:
        url_list = reverse('list')
        self.response = self.client.get(url_list)

    def test_form_template(self):
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'employee_list.html')

