from django.shortcuts import render, redirect
from .forms import EmployeeForm
from .models import Employee


def empl_list(request):
    context = Employee.objects.all()
    return render(request, 'employee_list.html', {'employee_list': context})


def empl_form(request, empl_id=0):
    if request.method == 'GET':
        if empl_id == 0:
            form = EmployeeForm()
        else:
            employee = Employee.objects.get(pk=empl_id)
            form = EmployeeForm(instance=employee)
        return render(request, 'employee_form.html', {'form': form})
    else:
        if empl_id == 0:
            form = EmployeeForm(request.POST)
        else:
            employee = Employee.objects.get(pk=empl_id)
            form = EmployeeForm(request.POST, instance=employee)
        if form.is_valid():
            form.save()
        return redirect('list')


def empl_delete(request, empl_id):
    employee = Employee.objects.get(pk=empl_id)
    employee.delete()
    return redirect('list')
