from django import forms
from .models import Employee


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = ('employee_code', 'fullname', 'phone_number', 'position')
        labels = {
            'employee_code': 'EMP code',
            'fullname': 'Full name',
            'phone_number': 'mobile'
        }

    def __init__(self, *args, **kwargs):
        super(EmployeeForm, self).__init__(*args, **kwargs)
        self.fields['position'].empty_label = 'Select'
        self.fields['employee_code'].requared = False
