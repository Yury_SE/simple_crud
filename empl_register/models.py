from django.db import models
from django.db import models
from django.utils.translation import gettext_lazy as _


class Position(models.Model):
    class EnumPositions(models.TextChoices):
        JUNIOR = 'Junior', _('Junior Developer')
        MIDDLE = 'Middle', _('Middle Developer')
        SENIOR = 'Senior', _('Senior Developer')
        LEAD = 'TL', _('Team Lead')
        ARCHITECT = 'AR', _('Architect')

    title = models.CharField(
        max_length=20,
        choices=EnumPositions.choices,
        default=EnumPositions.JUNIOR,
    )

    def __str__(self):
        return self.title


class Employee(models.Model):
    fullname = models.CharField(max_length=100)
    employee_code = models.CharField(max_length=8)
    phone_number = models.CharField(max_length=100)
    position = models.CharField(_('title'), choices=Position.EnumPositions.choices,
                                default=Position.EnumPositions.JUNIOR, max_length=20)
